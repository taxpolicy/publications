# Tax policy publications

This project includes the final versions and any supporting documents for all documents published on https://taxpolicy.ird.govt.nz.

For each document there are folders (organised by years) with:

* An INFO file: this is a plain text file that includes information about a publication, such as the title, date published, description, web site address, and so on.
* Publication files in different formats: this may include the source document(s) (such as the Microsoft Word file), the PDF published on the website, PDF archive (PDF/A) versions of files, and source files for images (such as a Microsoft Visio file and SVG files).

## Folder and file naming

### Folder organisation

There is a folder for each document, organised using this structure: 

| Folder | Description |
| ---    | ----------  |
| YYYY   |  A folder for each year |
| &nbsp; publication-folder | A folder for each document, named using the file naming conventions |
|  &nbsp; &nbsp; **subfolders** | Subfolders may be used if required to help organise multiple documents - generally there is only one version of a publication |
| &nbsp; &nbsp;  v1 | Where there are multiple versions of a publication - v1, v2, ... |

The publication folder will include multiple files in different formats, including:
* doc: Microsoft Word document(s)
* docx: Microsoft Word document(s)
* html: Web page files including image and HTML files
* images: images in various formats, including gif, png, jpeg, and svg
* pdf: Standard PDFs
* pdf (PDF/A): PDFs in PDF/A format for long-term preservation
* ZIP file: Abbyy Finreader source files in a ZIP file - only used for earlier documents scanned and processed using Abbyy FineReader (now called Abbyy FineReader PDF)

### Folder and file naming

This covention is used for naming folders and files for documents, and is also used as part of the web page address:

 **YYYY-document-type-short-title**, for example 2019-or-ksslrm-bill.

Where:

* YYYY = the year the document was published

* document-type = a code for the differet types of documents:
  * act = an amendment Act
  * bill = a bill
  * commentary = a commentary on new bills introduced into Parliament
  * dd = a Government discussion document with proposals for tax policy reform
  * ip = an officials' issue paper with officials' proposals for tax policy reform
  * ir = an information release, typically proactively releases of Cabinet papers and supporting material
  * or = an officials' report on submissions on a bill
  * other = documents that do not fit into any other category
  * ria or ris = a regulatory impact assessment (previously called regulatory impact statements)
  * sop = a Supplementary Order Paper
  * sr = a special report on new legislation recently enacted (an early version of the *Tax Information Bulletin* item)

* short-title = a short title for a publication

See https://taxpolicy.ird.govt.nz/site-map for more details of the different types of documents.

Additional naming conventions:

* No spaces: there are no spaces in folder and file names
* Dashes to separate words: a dash (-) is used to separate words
* Lowercase: all folders and file names are in lowercase
* Image files: where there are multiple image files a number and a short description are added to the end of file names, for example: 2019-or-ksslrm-bill-0-cover.jpg, 2019-or-ksslrm-bill-1-figure-1.svg
* Use leading zeros: leading zeros are added where files require numbering and there are more than nine numbers in a sequence
* Dates: the YYYY-MM-DD format is used when dates are requried as part of the file name
* Image source file: where Visio, PowerPoint or other tools are used to create images, -diagrams is added to the end of the file name
* Archive PDFs: the PDF/A format is indicated by adding -archive(pdf-a) to the end of the file name

## Plans

Plans for this project include:

* Progressively adding all tax policy documents.
* Providing a master index of all tax policy documents in a spreadsheet, including Microsoft Excel, LibreOffice, and CSV formats.
* Providing a MARC file(s) for all documents, for use in adding documents to library management systems.
* Adding all document information to wikidata.org
* Adding all documents to the Internet Archive
* Adding entries for all documents to the https://openlibrary.org

## Licence

Unless indicated otherwise for specific items the documents are licensed for re-use under a [Creative Commons Attribution 4.0 International licence](https://creativecommons.org/licenses/by/4.0/).

In essence, you are free to copy, distribute and adapt the material, as long as you attribute it to Inland Revenue and abide by the other licence terms. Please note that this licence does not apply to any logos, emblems and trade marks on the website. Those specific items may not be re-used without express permission.

Exceptions:

* There is no copyright on any New Zealand legislation (this includes Acts, Bills, and Supplementary Order Papers).
* There is no copyright on documents resulting from New Zealand Government appointed reviews and committees, such as early consultation documents and reports from consulative committees.
* Material that is protected by the copyright of a third party (such as submissions on consultation documents that are publicly released).

See https://taxpolicy.ird.govt.nz/copyright-policy for more information.

## Contact

Email policy.webmaster@ird.govt.nz if you have any questions about this project and any of the documents.
